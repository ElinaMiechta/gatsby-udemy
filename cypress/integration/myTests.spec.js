describe("Testing the Tag Suit", () => {
  it("test count of tag items", () => {
    cy.visit("/tags")

    cy.get("main").find(".tags-page").find("a").should("have.length", 7)
  })

  it("should check amount of tagged items", () => {
    cy.visit("/tags")

    const items = [0, 1, 2, 3, 4, 5]

    cy.wrap(items).each(count => {
      cy.get("main")
        .find(".tags-page")
        .find("a")
        .then(a => {
          const breakfast = a.eq(count)
          const amount = breakfast.find("p").text().split(" ")[0]

          cy.wrap(breakfast).click()
          cy.wait(500)
          cy.get("main")
            .find(".tag-recipes")
            .find(".recipes-list")
            .find("a")
            .should("have.length", amount)
          cy.get(".navbar .nav-center")
            .find(".nav-links")
            .find("a")
            .contains("tags")
            .click()
        })
    })
  })

  it("should check for title", () => {
    cy.visit("/tags")

    const foodItems = [0, 1, 2]

    const tagTitle = cy
      .get("main")
      .find(".tags-page")
      .find("a")
      .eq(3)
      .find("h5")
      .invoke("text")

    tagTitle.should("equal", "food")

    cy.get("main").find(".tags-page").find("a").eq(3).click()
    cy.wait(500)
    cy.wrap(foodItems).each(count => {
      cy.wait(500)
      cy.get("main")
        .find(".tag-recipes")
        .find(".recipes-list")
        .find("a")
        .then(a => {
          const chosenItem = a.eq(count)

          const title = chosenItem.find("h5").text()

          cy.wrap(chosenItem).click()
          cy.wait(500)
          const titleFromTagsPage = cy
            .get("main")
            .find(".recipe-page .recipe-hero")
            .find(".recipe-info")
            .find("h2")

          titleFromTagsPage.should("contain", title)
          cy.get(".recipe-info")
            .find(".recipe-tags")
            .find("a")
            .contains("food")
            .click()
        })
    })
  })

  it("Contact Page Check", () => {
    cy.visit("/")

    cy.get(".navbar .nav-center")
      .find(".nav-links")
      .find(".contact-link a")
      .click()
    cy.get("form")
      .find("div")
      .then(formRow => {
        const name = formRow.eq(0)
        const email = formRow.eq(1)
        const msg = formRow.eq(2)

        cy.wrap(name).find("#name").type("John")
        cy.wrap(email).find("#email").type("test@email.com")
        cy.wrap(msg).find("#message").type("Hello there from cypress")
      })

    cy.get("form").find("button").click()
  })
})
