import React from "react"
import TagsList from "./TagsList"
import RecepiesList from "./RecepiesList"
import { useStaticQuery, graphql } from "gatsby"

const query = graphql`
  {
    allContentfulRecepie(sort: { fields: title, order: ASC }) {
      nodes {
        id
        title
        cookTime
        prepTime
        content {
          tags
        }
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
        }
      }
    }
  }
`

const AllRecepies = () => {
  const data = useStaticQuery(query)
  const recipes = data.allContentfulRecepie.nodes

  return (
    <section className="recipes-container">
      <TagsList recipes={recipes} />
      <RecepiesList recipes={recipes} />
    </section>
  )
}

export default AllRecepies
