import React from "react"

const Footer = () => {
  return (
    <footer className="page-footer">
      <p>
        &copy; {new Date().getFullYear()} <span>SimpleRecepies</span> Build with{" "}
        <a href="https://www.gatsbyjs.com/" target="__self">
          Gatsby
        </a>
      </p>
    </footer>
  )
}

export default Footer
