import React from "react"
import Layout from "../components/Layout"

const Error = () => {
  return (
    <Layout>
      <main className="error-page">
        <section>
          <h1>404</h1>
          <h3>page does not exist</h3>
        </section>
      </main>
    </Layout>
  )
}

export default Error
