import React from "react"
import AllRecepies from "../components/AllRecepies"
import Layout from "../components/Layout"
import SEO from "../components/SEO"

const Recepies = () => {
  return (
    <Layout>
      <SEO title="Recipes" />
      <main className="page">
        <AllRecepies />
      </main>
    </Layout>
  )
}

export default Recepies
