/* Creating Tag Page Template */
import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/Layout"
import RecepiesList from "../components/RecepiesList"
import SEO from "../components/SEO"

const TagTemplate = ({ data, pageContext: { tag } }) => {
  const recipies = data.allContentfulRecepie.nodes
  return (
    <Layout>
      <SEO title={tag} />
      <main className="page">
        <h2>{tag}</h2>
        <div className="tag-recipes">
          <RecepiesList recipes={recipies} />
        </div>
      </main>
    </Layout>
  )
}

export const query = graphql`
  query getTaggedRecipies($tag: String) {
    allContentfulRecepie(
      sort: { fields: title, order: ASC }
      filter: { content: { tags: { eq: $tag } } }
    ) {
      nodes {
        id
        title
        prepTime
        cookTime
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: TRACED_SVG)
        }
      }
    }
  }
`

export default TagTemplate
